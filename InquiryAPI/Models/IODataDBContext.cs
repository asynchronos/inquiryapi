﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InquiryAPI.Models
{
    public interface IODataDBContext
    {
        IDbSet<Transaction> Transactions { get; set; }
    
        int SaveChanges();

        Task<int> SaveChangesAsync();

        DbEntityEntry Entry(object entity);

        DbEntityEntry<TEntityType> Entry<TEntityType>(TEntityType entity) where TEntityType : class;

        void Dispose();

        void SetModified(object entity);
    }
}
