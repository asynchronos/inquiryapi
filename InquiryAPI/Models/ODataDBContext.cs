﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InquiryAPI.Models
{
    public partial class ODataDBContext : DbContext, IODataDBContext
    {
        static ODataDBContext()
        {
            Database.SetInitializer<ODataDBContext>(null);
        }

        public ODataDBContext() : base("Name=ODataDBContext") { }

        public ODataDBContext(string connectionstring) : base(connectionstring) { }

        public IDbSet<Transaction> Transactions {get;set;}

        public void SetModified(object entity)
        {
            throw new NotImplementedException();
        }
    }
}