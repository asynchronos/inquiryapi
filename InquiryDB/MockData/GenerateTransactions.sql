
--USE [Inquiry]
--GO

--/****** Object:  Table [dbo].[random_sender_receiver]    Script Date: 12/19/2018 7:10:00 PM ******/
--DROP TABLE [dbo].[random_sender_receiver]
--GO

--/****** Object:  Table [dbo].[random_sender_receiver]    Script Date: 12/19/2018 7:10:00 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[random_sender_receiver](
--	[trans_id] int IDENTITY(1,1) NOT NULL,
--	[sender] NUMERIC(10,0) NOT NULL,
--	[receiver] NUMERIC(10,0) NOT NULL,
--  [AMT] NUMERIC(18,2) NOT NULL,
--  [rand_date] datetime NOT NULL,
--  [ccy] nvarchar(3) NOT NULL,
--  [status] int NOT NULL

--) ON [PRIMARY]

--GO

--delete random_sender_receiver
-------------------------------------------------------------------------

--select * from random_sender_receiver

/*
-- random map sender & receiver
insert into random_sender_receiver(sender,receiver,amt,rand_date,ccy,[status])
select r.sender, r.receiver, r.amt, r.rand_date, ccy.[Currency_Code],r.status
from (
	select FLOOR(Rand()*500) as sender, FLOOR(Rand()*500) as receiver
		,CONVERT(NUMERIC(18,2),FLOOR(RAND()*10)*RAND()*RAND()*1000)/RAND() as amt
		,DATEADD(day, ROUND(DATEDIFF(day, '2018-01-01', GETDATE()) * RAND(CHECKSUM(NEWID())), 0)
			,DATEADD(second, CHECKSUM(NEWID()) % 48000, '2018-01-01')) as rand_date
		,(select FLOOR(RAND()*COUNT(*)) as rand_seq from [Inquiry].[dbo].Currencies) as ccy_seq
		,FLOOR(RAND()*3)+1 as [status]
) r inner join
(
	SELECT [Currency_Code]
		,row_number() over(order by currency_code) seq
	  FROM [Inquiry].[dbo].[Currencies]
) ccy on r.ccy_seq=ccy.seq
where sender<>receiver
	and amt <> 0
	and exists(
		select customer_id
		from customers sender
		where sender.customer_id=sender
	)and exists(
		select customer_id
		from customers receiver
		where receiver.customer_id=receiver
	)

waitfor delay '00:00:00:001'
GO 1000 -- remove comment for run loop
;
*/

/*

-- remove dup
delete random_sender_receiver
where exists(
	select sender,receiver
	from (
		select sender,receiver
		from random_sender_receiver
		group by sender,receiver
		having count(*)>1
	) dup
	where dup.sender=random_sender_receiver.sender
		and dup.receiver=random_sender_receiver.receiver
) 
;
select * from random_sender_receiver
;
-- insert transaction
delete [dbo].[Transactions];
SET IDENTITY_INSERT dbo.[Transactions] ON;  

INSERT INTO [dbo].[Transactions]
           ([Trans_ID]
           ,[Sender_ID]
           ,[Receiver_ID]
           ,[Trans_Date]
           ,[Amount]
           ,[Currency_Code]
           ,[Status_Code])
select r.trans_id,r.sender,r.receiver,r.rand_date, r.amt, r.ccy,r.[status]
from random_sender_receiver r
;
SET IDENTITY_INSERT dbo.[Transactions] OFF;  

*/