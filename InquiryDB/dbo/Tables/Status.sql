﻿CREATE TABLE [dbo].[Status] (
    [Status_Code]   INT           IDENTITY (1, 1) NOT NULL,
    [Status_Desc]   NVARCHAR (20) NOT NULL,
    [Is_Disable]    BIT           CONSTRAINT [DF_Status_Is_Disable] DEFAULT ((0)) NOT NULL,
    [Last_Modified] DATETIME      CONSTRAINT [DF_Status_Last_Modified] DEFAULT (getdate()) NOT NULL,
    [Modified_By]   NVARCHAR (50) CONSTRAINT [DF__Status__Modified__22AA2996] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED ([Status_Code] ASC)
);

