﻿CREATE TABLE [dbo].[Transactions] (
    [Trans_ID]      INT             IDENTITY (1, 1) NOT NULL,
    [Sender_ID]     NUMERIC (10)    NOT NULL,
    [Receiver_ID]   NUMERIC (10)    NOT NULL,
    [Trans_Date]    DATETIME        NOT NULL,
    [Amount]        DECIMAL (18, 2) NOT NULL,
    [Currency_Code] NVARCHAR (3)    NOT NULL,
    [Status_Code]   INT             NOT NULL,
    [Last_Modified] DATETIME        CONSTRAINT [DF_Transactions_Last_Modified] DEFAULT (getdate()) NOT NULL,
    [Modified_By]   NVARCHAR (50)   CONSTRAINT [DF__Transacti__Modif__1367E606] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED ([Trans_ID] ASC),
    CONSTRAINT [FK_Transactions_Currencies1] FOREIGN KEY ([Currency_Code]) REFERENCES [dbo].[Currencies] ([Currency_Code]),
    CONSTRAINT [FK_Transactions_Customers] FOREIGN KEY ([Sender_ID]) REFERENCES [dbo].[Customers] ([Customer_ID]),
    CONSTRAINT [FK_Transactions_Customers1] FOREIGN KEY ([Receiver_ID]) REFERENCES [dbo].[Customers] ([Customer_ID]),
    CONSTRAINT [FK_Transactions_Status] FOREIGN KEY ([Status_Code]) REFERENCES [dbo].[Status] ([Status_Code])
);

