﻿CREATE TABLE [dbo].[Currencies] (
    [Currency_Code] NVARCHAR (3)   NOT NULL,
    [Number]        NVARCHAR (3)   NOT NULL,
    [Currency]      NVARCHAR (100) NOT NULL,
    [Country]       NVARCHAR (100) NOT NULL,
    [Is_Disable]    BIT            CONSTRAINT [DF_Currencies_Is_Disable] DEFAULT ((0)) NOT NULL,
    [Last_Modified] DATETIME       CONSTRAINT [DF_Currencies_Last_Modified] DEFAULT (getdate()) NOT NULL,
    [Modified_By]   NVARCHAR (50)  CONSTRAINT [DF__Currencie__Modif__286302EC] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED ([Currency_Code] ASC)
);

