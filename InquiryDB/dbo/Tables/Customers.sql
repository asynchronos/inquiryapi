﻿CREATE TABLE [dbo].[Customers] (
    [Customer_ID]   NUMERIC (10)  NOT NULL,
    [Customer_Name] NVARCHAR (30) NOT NULL,
    [Contact_Email] NVARCHAR (25) NOT NULL,
    [Mobile_No]     NUMERIC (10)  NOT NULL,
    [Is_Disable]    BIT           CONSTRAINT [DF_Customers_Is_Disable] DEFAULT ((0)) NOT NULL,
    [Last_Modified] DATETIME      CONSTRAINT [DF_Customers_Last_Modified] DEFAULT (getdate()) NOT NULL,
    [Modified_By]   NVARCHAR (50) CONSTRAINT [DF_Customers_Modified_By] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([Customer_ID] ASC)
);

