﻿CREATE TABLE [dbo].[random_sender_receiver] (
    [trans_id]  INT             IDENTITY (1, 1) NOT NULL,
    [sender]    NUMERIC (10)    NOT NULL,
    [receiver]  NUMERIC (10)    NOT NULL,
    [AMT]       NUMERIC (18, 2) NOT NULL,
    [rand_date] DATETIME        NOT NULL,
    [ccy]       NVARCHAR (3)    NOT NULL,
    [status]    INT             NOT NULL
);

